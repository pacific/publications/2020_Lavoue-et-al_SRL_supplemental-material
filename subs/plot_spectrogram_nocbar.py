#
# Plot the spectrogram of a trace using scipy.
#
# Francois Lavoue (francois.lavoue@univ-grenoble-alpes.fr), 20 Nov 2019
# Last updated FL, 27 Nov 2019
# Copyright (c) 2019, Univ. Grenoble Alpes

import numpy as np
import matplotlib.pyplot as plt
from scipy import signal as scisignal
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.ticker as mticker

def plot_spectrogram(trace, ax, inputs) :
    
    # input parameters
    dt = inputs.dt
    t_win = inputs.t_win   # time window for spectrogram (s)
    t_overlap = inputs.t_overlap
    fmax_plot = inputs.fmax_plot   # (Hz)
    tmax_plot = inputs.tmax_plot   # (s)
    Arange_plot = inputs.Arange_plot   # amplitude range (dB)
    cmap = inputs.cmap   # colormap (e.g. "gray_r" or "viridis")
    flag_plot_time_windows = inputs.flag_plot_time_windows
    flag_plot_frequency_lines = inputs.flag_plot_frequency_lines
    f1 = inputs.f1
    f2 = inputs.f2

    n_win = np.int_(np.round(t_win/dt))
    n_overlap = np.int_(np.round(t_overlap/dt))
    print(n_win, n_overlap)

    # sampling frequency
    fs = 1/dt
    print("dt =", dt)
    print("fs =", fs)

    # compute spectrogram
    vf_spec, vt_spec, spectrogram = scisignal.spectrogram(trace, fs=1/dt, \
                                    nperseg=n_win, noverlap=n_overlap, scaling='density', mode='psd')
    #  scipy.trace.spectrogram(x, fs=1.0, \
    #        window=('tukey', 0.25), nperseg=None, noverlap=None, nfft=None, detrend='constant', \
    #        return_onesided=True, scaling='density', axis=-1, mode='psd')

    # convert to dB
    spectrogram_dB = 10*np.log10(spectrogram)

    print("spectrogram min/max = %g / %g"    % (spectrogram.min(), spectrogram.max()))
    print("                    = %g / %g dB" % (spectrogram_dB.min(), spectrogram_dB.max()))

    Amax_plot = np.floor(spectrogram_dB.max())
    Amin_plot = Amax_plot - Arange_plot
    dA_leg = np.int_(np.round(Arange_plot/5))
    yticks = np.arange(Amin_plot,Amax_plot+0.1*dA_leg,dA_leg)

    # plot
    plot = ax.pcolormesh(vt_spec, vf_spec, spectrogram_dB, vmin=Amin_plot, vmax=Amax_plot, cmap=cmap)

    # plot overlapping time windows in which we compute FFTs
    if flag_plot_time_windows :
       t0_plot_win = t_win
       ax.plot([t0_plot_win,t0_plot_win]                                                 , [0.0,fmax_plot], 'k--', linewidth=1)
       ax.plot([t0_plot_win+t_win                  , t0_plot_win+t_win                  ], [0.0,fmax_plot], 'k--', linewidth=1)
       ax.plot([t0_plot_win+(t_win-t_overlap)      , t0_plot_win+(t_win-t_overlap)      ], [0.0,fmax_plot], 'k--', linewidth=1, dashes=(3,5))
       ax.plot([t0_plot_win+(t_win-t_overlap)+t_win, t0_plot_win+(t_win-t_overlap)+t_win], [0.0,fmax_plot], 'k--', linewidth=1, dashes=(3,5))

    # plot fundamental frequencies
    k1 = 3
    if flag_plot_frequency_lines :
        ax.plot([0.0,tmax_plot], [f1,f1], "--", dashes=(7,7), color="C2", linewidth=1, label=("$f_1 = %g$ Hz"%f1))
        ax.plot([0.0,tmax_plot], [f2,f2], "--", dashes=(3,3), color="C3", linewidth=1.5, label=("$f_2 = %g$ Hz"%f2))
        for imode in range(2,200) :
            ax.plot([0.0,tmax_plot], [imode*f1,imode*f1], "--", dashes=(7,7), color="C2", linewidth=1)
            ax.plot([0.0,tmax_plot], [imode*f2,imode*f2], "--", dashes=(3,3), color="C3", linewidth=1.5)
        ax.legend(loc="lower right", bbox_to_anchor=[1.0,1.0], ncol=5)

    # label
    #ax.set_title('Spectrogram')
    ax.set_ylabel('Frequency (Hz)')
    ax.set_xlabel('Time (s)')

    cbar = []
    '''
    # colorbar
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(plot, cax=cax, orientation='vertical')
    #cbar.set_label("Amplitude")
    #cbar.set_label("Amplitude (log)")
    cbar.set_label("Power spectral density (dB/Hz)")

    ## colorbar ticks in 10**k format
    #cbar.ax.set_yticks(yticks)
    #f = mticker.ScalarFormatter(useOffset=False, useMathText=True)
    #g = lambda x,pos : "${}$".format(f._formatSciNotation('%1.10e' % x))
    #fmt = mticker.FuncFormatter(g)
    ##cby_ticks = [ "{}".format(fmt(val)) for val in 10**np.arange(Amin_plot,Amax_plot+0.1*dA_leg,dA_leg) ]
    #cby_ticks = [ "{}".format(fmt(val)) for val in 10**cbar.ax.get_yticks() ]
    #cbar.ax.set_yticklabels(cby_ticks)
    '''

    # tune
    #ax.set_yscale('log')
    ax.set_ylim(0.0,fmax_plot)
    ax.set_xlim(0.0,tmax_plot)
    
    return plot, cbar

#-- end function plot_spectrogram

