# ----------------------------------------------------------------------

# Copyright 2008-2011 SEISCOPE project, All rights reserved.  
# Copyright 2013-2020 SEISCOPEII project, All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#    *  Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    *  Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in
#       the documentation and/or other materials provided with the
#       distribution.
#    *  Neither the name of the SEISCOPE project nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# Warranty Disclaimer:  
# THIS SOFTWARE IS PROVIDED BY THE SEISCOPE PROJECT AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# SEISCOPE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Acknowledgements:  
# SEISCOPE project's codes have been developed in the framework of
# SEISCOPE and SEISCOPE 2 consortia, and we thank the sponsors of
# these projects.

# ----------------------------------------------------------------------

"""
Part of SEISCOPE_PyTools which manages I/O
"""

# import pre-required modules
from copy import deepcopy
import numpy as np

# ----------------------------------------------------------------------


def SEM46_acquisition(filename):
    """
    Read an acquisition file used in SEM46 or TOYxDAC

    Author: Theodosius Marwan Irnaka (theodosius-marwan.irnaka@univ-grenoble-alpes.fr), 2019

    Parameters
    ----------
    filename : str
        acquisition filename

    Returns
    -------
    source : Points
        Source data Points
    receiver : list
        list of receiver data Points
    """

    from seiscope_pytools.data.spatial import Points

    src = [[],[],[]]
    rec = []

    with open(filename,'r') as fi:
        newid = 1
        srcid = -1

        for ln in fi:
            splitline = ln.split()
            oldid = deepcopy(newid)
            newid = int(splitline[-1])

            if (newid==0 and oldid==1):
                rec.append([[],[],[]])
                srcid += 1
            if newid==0:
                src[0].append(float(splitline[0]))
                src[1].append(float(splitline[1]))
                src[2].append(float(splitline[2]))
            elif newid==1:
                rec[srcid][0].append(float(splitline[0]))
                rec[srcid][1].append(float(splitline[1]))
                rec[srcid][2].append(float(splitline[2]))

    # put them on the correct class
    source = Points((src[1],src[2],src[0]))
    receiver = []
    for isrc in range(source.npoints):
        receiver.append(Points((rec[isrc][1],rec[isrc][2],rec[isrc][0])))

    return source,receiver
    # return np.array(src),np.array(rec)

# ------ end function SEM46_acquisition --------------------------------


# ----------------------------------------------------------------------


def read_simulation_parameters(list_of_files, list_of_pattern_lists=None) :

    """
    Read simulation parameters from SEM46 input files.

    Author: Francois Lavoue (francois.lavoue@univ-grenoble-alpes.fr), 10 July 2019  
    Last updated Francois Lavoue, 22 July 2019  
    Copyright (c) 2019, Univ. Grenoble Alpes

    Parameters
    ----------
    list_of_files : list(str)
        List of SEM46 input files from which variables will be read from
        i.e. mesh_input_namelist, projection_input_name_list, and/or
        sem_input_list.
        If a file is missing, the corresponding variables will be left
        to the default None value (see seiscope_pytools.data.sem46).

    list_of_pattern_lists : list( list(str) )
        List of lists of patterns to search for in each input file.
        (as many pattern lists as input files)
        NB: you don't have to know these patterns a priori, default
            patterns are defined below.

    Returns
    -------
    mesh_param : Mesh_Param
        Mesh parameters read from mesh_input_name_list
    proj_param : Projection_Param
        Projection parameters read from projection_input_name_list
    sem_param : Sem_Param
        SEM parameters read from sem_input_name_list

    Examples
    -------
    file1 = dir_in + "/mesh_input_namelist"
    file2 = dir_in + "/projection_input_namelist"
    file3 = dir_in + "/sem_input_namelist"

    # read all input variables
    list_of_files = [ file1, file2, file3 ]
    mp, pp, sp = reads.read_simulation_parameters(list_of_files)

    # read only variables related to acquisition in projection_input_namelist
    # and sem_input_namelist (all other variables will be left to default None)
    list_of_files = [ file2, file3 ]
    list_of_pattern_lists = [ [ '&ACQUISITION' ], [ '&ACQUISITION_INFO' ] ]
    mp, pp, sp = reads.read_simulation_parameters(list_of_files, list_of_pattern_lists=list_of_pattern_lists)

    Notes
    -------
    - This function relies on the indexing of line.split() lists to find
      variables, and therefore assumes
      1. that variables are always given in the same order,
      2. that lines are not broken (i.e. all variables are on a single
         line for a given pattern).

    To do
    -------
    - Also read Fwi_Param from fwi_input_namelist.
    - Also read Output_Param from standard output logs? (maybe in another function)

    """

    from seiscope_pytools.data import sem46
    from os.path import basename
    from sys import exit

    #-- init.
    mesh_param = sem46.Mesh_Param()
    proj_param = sem46.Projection_Param()
    sem_param = sem46.Sem_Param()

    if list_of_pattern_lists == None :
        list_of_pattern_lists = ['']*len(list_of_files)

    #-- loop over input files
    for (file_name,pattern_list) in zip(list_of_files, list_of_pattern_lists) :

        # read lines in file
        file_in = open(file_name, 'r')
        lines = file_in.readlines()
        file_in.close()

        # define pattern list if not provided in input
        if len(pattern_list) < 1 :
            if basename(file_name) == 'mesh_input_namelist' :
                pattern_list = [ '&MESH_INFO', '&BOUNDARY_ABC', '&MESH_HOMO', '&MESH_VARY', '&AUTOMATIC_MESH', '&PREDESIGN_MESH', '&TOPO' ]

            elif basename(file_name) == 'projection_input_namelist' :
                #pattern_list = [ '&WORKING_OPT', '&FILE', '&REGULAR_GRID', '&DOMAIN_DECOMPOSITION', '&ACQUISITION' ]
                pattern_list = [ '&WORKING_OPT', '&REGULAR_GRID', '&ACQUISITION' ]
                # (do not include '&FILE' as it may appear several times,
                # nor '&DOMAIN_DECOMPOSITION' as ti also appears in sem_input_name_list)

            elif basename(file_name) == 'sem_input_namelist' :
                pattern_list = [ '&WORKING_MODE', '&ACQUISITION_INFO', '&SOURCE_INFO', '&SOURCE_TYPE', '&DOMAIN_DECOMPOSITION', \
                                 '&FILE_NAME_MODEL', '&FORWARD_PARAM', '&SEISMO', '&SNAPSHOT', '&ATTENUATION_INFO', '&UNKNOWN_PARAM' ]

        #-- loop over patterns
        for pattern in pattern_list :

            nfound = 0

            #-- loop over lines in file to search for pattern
            for line in lines :
                if pattern in line:

                    # reformat line
                    line = line.strip().replace(',',' ').replace('/','').split()
                    # (replace "," by " " to make sure to well separate variables,
                    # and remove "/" at the end of the line)

                    # skip lines that do not start by pattern
                    if line[0] != pattern :
                        continue

                    # check that we find only one line for each pattern
                    nfound = nfound + 1
                    if nfound > 1 :
                        print("Error: pattern '%s' found more than once in '%s'." % (pattern,file_name))
                        exit("Error: pattern found more than once.")

                    # read variables from line
                    variables = line[1::]


                    # find patterns for Mesh_Param
                    if pattern == '&MESH_INFO' :
                        mesh_param.mesh_option = np.int_(variables[0].replace('mesh_option=',''))
                        mesh_param.IS_variable_mesh = np.int_(variables[1].replace('IS_variable_mesh=',''))
                        mesh_param.mesh_name = variables[2].replace('mesh_name=','')
                        mesh_param.ISvtk_mesh = np.int_(variables[3].replace('ISvtk_mesh=',''))
                        mesh_param.sponge_length = np.int_(variables[4].replace('sponge_length=',''))

                    elif pattern == '&BOUNDARY_ABC' :
                        mesh_param.iABC = np.int_(variables[0].replace('iABC=',''))

                    elif pattern == '&MESH_HOMO' :
                        mesh_param.Nb_elementZ = np.int_(variables[0].replace('Nb_elementZ=',''))
                        mesh_param.Nb_elementX = np.int_(variables[1].replace('Nb_elementX=',''))
                        mesh_param.Nb_elementY = np.int_(variables[2].replace('Nb_elementY=',''))
                        mesh_param.Nb_deformed_element = np.int_(variables[3].replace('Nb_deformed_element=',''))
                        mesh_param.dZ = np.float(variables[4].replace('dZ=',''))
                        mesh_param.dX = np.float(variables[5].replace('dX=',''))
                        mesh_param.dY = np.float(variables[6].replace('dY=',''))

                    elif pattern == '&MESH_VARY' :
                        if mesh_param.IS_variable_mesh == 1 :
                        # (if not, values in input file may not be relevant, so keep default values)
                            mesh_param.sizeZ = np.float(variables[0].replace('sizeZ=',''))
                            mesh_param.sizeX = np.float(variables[1].replace('sizeX=',''))
                            mesh_param.sizeY = np.float(variables[2].replace('sizeY=',''))
                            mesh_param.IS_predesigned_mesh = np.int_(variables[3].replace('IS_predesigned_mesh=',''))
                            mesh_param.deformation_ratio = np.float(variables[4].replace('deformation_ratio=',''))
                            mesh_param.inter_element_size_name = variables[5].replace('inter_element_size_name=','')
                            mesh_param.extra_mesh_info = variables[6].replace('extra_mesh_info=','')

                    elif pattern == '&AUTOMATIC_MESH' :
                        if mesh_param.IS_variable_mesh == 1 and mesh_param.IS_predesigned_mesh == 0 :
                        # (if not, values in input file may not be relevant, so keep default values)
                            mesh_param.nb_points_per_shortest_wavelength = np.float(variables[0].replace('nb_points_per_shortest_wavelength=',''))
                            mesh_param.frequency_max = np.float(variables[1].replace('frequency_max=',''))
                            mesh_param.maxV_homo = np.float(variables[2].replace('maxV_homo=',''))
                            mesh_param.minV_homo = np.float(variables[3].replace('minV_homo=',''))
                            mesh_param.maxV_name = variables[4].replace('maxV_name=','')
                            mesh_param.minV_name = variables[5].replace('minV_name=','')
                            mesh_param.Nb_point_Z = np.int_(variables[6].replace('Nb_point_Z=',''))
                            mesh_param.Nb_point_X = np.int_(variables[7].replace('Nb_point_X=',''))
                            mesh_param.Nb_point_Y = np.int_(variables[8].replace('Nb_point_Y=',''))
                            mesh_param.dZ_reg = np.float(variables[9].replace('dZ_reg=',''))
                            mesh_param.dX_reg = np.float(variables[10].replace('dX_reg=',''))
                            mesh_param.dY_reg = np.float(variables[11].replace('dY_reg=',''))

                    elif pattern == '&PREDESIGN_MESH' :
                        if mesh_param.IS_variable_mesh == 1 and mesh_param.IS_predesigned_mesh == 1 :
                        # (if not, values in input file may not be relevant, so keep default values)
                            mesh_param.input_element_size_name = variables[0].replace('input_element_size_name=','')
                            mesh_param.maxV_homo = np.float(variables[1].replace('maxV_homo=',''))
                            mesh_param.maxV_name = variables[2].replace('maxV_name=','')
                            mesh_param.Nb_point_Z = np.int_(variables[3].replace('Nb_point_Z=',''))
                            mesh_param.Nb_point_X = np.int_(variables[4].replace('Nb_point_X=',''))
                            mesh_param.Nb_point_Y = np.int_(variables[5].replace('Nb_point_Y=',''))
                            mesh_param.dZ_reg = np.float(variables[6].replace('dZ_reg=',''))
                            mesh_param.dX_reg = np.float(variables[7].replace('dX_reg=',''))
                            mesh_param.dY_reg = np.float(variables[8].replace('dY_reg=',''))

                    elif pattern == '&TOPO' :
                        mesh_param.topo_name = variables[0].replace('topo_name=','')
                        mesh_param.topo_Nb_point_X = np.int_(variables[1].replace('Nb_point_X=',''))
                        mesh_param.topo_Nb_point_Y = np.int_(variables[2].replace('Nb_point_Y=',''))
                        mesh_param.topo_dX = np.float(variables[3].replace('dX=',''))
                        mesh_param.topo_dY = np.float(variables[4].replace('dY=',''))


                    # find patterns for Projection_Param
                    elif pattern == '&WORKING_OPT' :
                        proj_param.working_option = np.int_(variables[0].replace('working_option=',''))
                        proj_param.number_file = np.int_(variables[1].replace('number_file=',''))

                    elif pattern == '&FILE' :
                        continue   # skip for now (cannot handle several files)
                        proj_param.name_file_cart = variables[0].replace('name_file_cart=','')
                        proj_param.name_file_gll = variables[1].replace('name_file_gll=','')

                    elif pattern == '&REGULAR_GRID' :
                        proj_param.Nb_point_Z = np.int_(variables[0].replace('Nb_point_Z=',''))
                        proj_param.Nb_point_X = np.int_(variables[1].replace('Nb_point_X=',''))
                        proj_param.Nb_point_Y = np.int_(variables[2].replace('Nb_point_Y=',''))
                        proj_param.dZ = np.float(variables[3].replace('dZ=',''))
                        proj_param.dX = np.float(variables[4].replace('dX=',''))
                        proj_param.dY = np.float(variables[5].replace('dY=',''))
                        proj_param.ISvtk = np.int_(variables[6].replace('ISvtk=',''))

                    elif pattern == '&DOMAIN_DECOMPOSITION' :
                        continue   # skip (redundant with sem_input_namelist)
                        proj_param.Nb_domainX = np.int_(variables[0].replace('Nb_domainX=',''))
                        proj_param.Nb_domainY = np.int_(variables[1].replace('Nb_domainY=',''))

                    elif pattern == '&ACQUISITION' :
                        proj_param.acqui_in = variables[0].replace('acqui_in=','')
                        proj_param.acqui_out = variables[1].replace('acqui_out=','')
                        proj_param.acqui_x_origin = np.float(variables[2].replace('x_origin=',''))
                        proj_param.acqui_y_origin = np.float(variables[3].replace('y_origin=',''))


                    # find patterns for Sem_Param
                    elif pattern == '&WORKING_MODE' :
                        sem_param.mode = np.int_(variables[0].replace('mode=',''))

                    elif pattern == '&ACQUISITION_INFO' :
                        sem_param.acqui_type = np.int_(variables[0].replace('acqui_type=',''))
                        sem_param.x_origin = np.float(variables[1].replace('x_origin=',''))
                        sem_param.y_origin = np.float(variables[2].replace('y_origin=',''))
                        sem_param.acqui_file = variables[3].replace('acqui_file=','')
                        sem_param.ISshot_by_shot = np.int_(variables[4].replace('ISshot_by_shot=',''))
                        sem_param.ISsame_receiver = np.int_(variables[5].replace('ISsame_receiver=',''))

                    elif pattern == '&SOURCE_INFO' :
                        sem_param.source_file_name = variables[0].replace('source_file_name=','')
                        sem_param.nb_sample = np.int_(variables[1].replace('nb_sample=',''))
                        sem_param.sourcepar = np.int_(variables[2].replace('sourcepar=',''))

                    elif pattern == '&SOURCE_TYPE' :
                        sem_param.ISFz = np.int_(variables[0].replace('ISFz=',''))
                        sem_param.ISFx = np.int_(variables[1].replace('ISFx=',''))
                        sem_param.ISFy = np.int_(variables[2].replace('ISFy=',''))
                        sem_param.M_zz = np.float(variables[3].replace('M_zz=',''))
                        sem_param.M_yy = np.float(variables[4].replace('M_yy=',''))
                        sem_param.M_xx = np.float(variables[5].replace('M_xx=',''))
                        sem_param.M_xz = np.float(variables[6].replace('M_xz=',''))
                        sem_param.M_xy = np.float(variables[7].replace('M_xy=',''))
                        sem_param.M_yz = np.float(variables[8].replace('M_yz=',''))

                    elif pattern == '&DOMAIN_DECOMPOSITION' :
                        sem_param.Nb_domainZ = np.int_(variables[0].replace('Nb_domainZ=',''))
                        sem_param.Nb_domainX = np.int_(variables[1].replace('Nb_domainX=',''))
                        sem_param.Nb_domainY = np.int_(variables[2].replace('Nb_domainY=',''))

                    elif pattern == '&FILE_NAME_MODEL' :
                        sem_param.Vp_homo = np.float(variables[0].replace('Vp_homo=',''))
                        sem_param.Vs_homo = np.float(variables[1].replace('Vs_homo=',''))
                        sem_param.Den_homo = np.float(variables[2].replace('Den_homo=',''))
                        sem_param.Vp_name = variables[3].replace('Vp_name=','')
                        sem_param.Vs_name = variables[4].replace('Vs_name=','')
                        sem_param.Den_name = variables[5].replace('Den_name=','')

                    elif pattern == '&FORWARD_PARAM' :
                        sem_param.dt = np.float(variables[0].replace('dt=',''))
                        sem_param.total_nb_time_step = np.int_(variables[1].replace('total_nb_time_step=',''))

                    elif pattern == '&SEISMO' :
                        sem_param.ISseisZ = np.int_(variables[0].replace('ISseisZ=',''))
                        sem_param.ISseisX = np.int_(variables[1].replace('ISseisX=',''))
                        sem_param.ISseisY = np.int_(variables[2].replace('ISseisY=',''))
                        sem_param.name_seis = variables[3].replace('name_seis=','')

                    elif pattern == '&SNAPSHOT' :
                        sem_param.ISnap = np.int_(variables[0].replace('ISnap=',''))
                        sem_param.snap_name_Z = variables[1].replace('snap_name_Z=','')
                        sem_param.snap_name_X = variables[2].replace('snap_name_X=','')
                        sem_param.snap_name_Y = variables[3].replace('snap_name_Y=','')
                        sem_param.direction = np.int_(variables[4].replace('direction=',''))
                        sem_param.step = np.int_(variables[5].replace('step=',''))

                    elif pattern == '&ATTENUATION_INFO' :
                        sem_param.min_freq = np.float(variables[0].replace('min_freq=',''))
                        sem_param.max_freq = np.float(variables[1].replace('max_freq=',''))
                        sem_param.Q_ref = np.float(variables[2].replace('Q_ref=',''))
                        sem_param.Qphomo = np.float(variables[3].replace('Qphomo=',''))
                        sem_param.Qshomo = np.float(variables[4].replace('Qshomo=',''))
                        sem_param.Qp_file = variables[5].replace('Qp_file=','')
                        sem_param.Qs_file = variables[6].replace('Qs_file=','')

                    elif pattern == '&UNKNOWN_PARAM' :
                        sem_param.icalcul = np.int_(variables[0].replace('icalcul=',''))
                        sem_param.ifreesurf = np.int_(variables[1].replace('ifreesurf=',''))

                    #-- end if pattern
                #-- end if pattern in line
            #-- end loop over lines in file to search for pattern

            # check that we well found the pattern
            if nfound != 1 :
                print("Error: pattern '%s' not found in '%s'." % (pattern,file_name))
                exit("Error: pattern not found.")

        #-- end loop over patterns in pattern list
    #-- end loop over input files

    return mesh_param, proj_param, sem_param

#-- end function read_simulation_parameters


# ----------------------------------------------------------------------


