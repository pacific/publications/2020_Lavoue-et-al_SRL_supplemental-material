# ----------------------------------------------------------------------

"""
Part of SEISCOPE_PyTools which manages data conversion.

"""

# import pre-required modules
import numpy as np

# ----------------------------------------------------------------------


def convert_SEM46_binary_seismo_to_Obspy_stream(file_in,infos) :

    '''
    Convert seismograms from SEM46 binary output files to Obspy streams.

    (still in progress but works as it is...)

    Francois Lavoue (francois.lavoue@univ-grenoble-alpes.fr), 10 June 2019  
    Last updated Francois Lavoue, 22 July 2019  
    Copyright (c) 2019, Univ. Grenoble Alpes

    Parameters
    ----------
    file_in : str
        Name of input binary file containing seismograms/traces.

    infos : structure containing the following header info
    infos.nt : int
    infos.dt : float
    infos.srcs : Points
    infos.recs : list(Points)

    Returns
    -------
    stream_list : list
        List of Obspy streams, containing one stream per source.

    Notes
    -------
    - The way we fill in the trace.stats is not standard and could
      evolve in the future. Here we seek compatibility with the headers
      of Pierre Boue's H5 files (see gricad-gitlab/bouep/pycorr).
    '''

    import obspy

    # read infos (must be given in input)
    nt = infos.nt
    dt = infos.dt
    comp = infos.comp
    srcs = infos.srcs
    recs = infos.recs

    # compute ntrace
    ntrace = 0
    for isrc in range(0,srcs.npoints) :
        ntrace = ntrace + recs[isrc].npoints

    # read seismogram from binary file
    data_in = np.fromfile(file_in, dtype=np.float32)
    data_in = np.reshape(data_in,(ntrace,nt)).transpose()

    # init. output list of streams
    stream_list = []
    itr = 0

    #-- build Obspy streams: 1 stream per source
    for isrc in range(0,srcs.npoints) :
        for irec in range(0,recs[isrc].npoints) :

            # select trace
            data = data_in[:,itr]

            # define header info
            channel = comp
            try :
                network = (srcs.id[isrc]).replace('.','-')
                station = (recs[isrc].id[irec]).replace('.','-')
            except :
                network = isrc
                station = irec
            # NB: IDs may not exist, not a Seiscope standard.

            # define trace with minimal header info
            trace = obspy.Trace(data, {"network": network, "station": station, "channel": channel, "delta": dt })
            # (https://docs.obspy.org/packages/autogen/obspy.core.trace.Stats.html#obspy.core.trace.Stats)

            # add custom info to trace stats
            trace.stats.comp = comp

            # create some empty dictionaries
            trace.stats.azimuths = {}
            trace.stats.coordinates = {}
            trace.stats.distances = {}
            trace.stats.IDs = {}

            trace.stats.coordinates['coord_src_xyz'] = [ srcs.X[isrc], srcs.Y[isrc], srcs.Z[isrc] ]
            trace.stats.coordinates['coord_rec_xyz'] = [ recs[isrc].X[irec], recs[isrc].Y[irec], recs[isrc].Z[irec] ]

            trace.stats.distances['dist_xyz'] = np.sqrt( (srcs.X[isrc] - recs[isrc].X[irec])**2 \
                                                       + (srcs.Y[isrc] - recs[isrc].Y[irec])**2 \
                                                       + (srcs.Z[isrc] - recs[isrc].Z[irec])**2 )

            trace.stats.azimuths['az_xyz'] = azimuth_xy( recs[isrc].X[irec]-srcs.X[isrc], recs[isrc].Y[irec]-srcs.Y[isrc], mode="180")

            try :
            # add src/rec IDs following pycorr conventions
            # (x2: original station ID + H5/correlations ID)
                trace.stats.IDs['id_src'] = srcs.id[isrc]
                trace.stats.IDs['id_rec'] = recs[isrc].id[irec]
                ids = np.float(srcs.id[isrc])
                idr = np.float(recs[isrc].id[irec])
                trace.stats.IDs['idvs'] = ( "%.2i.%.5i.00" % ( np.int_(np.floor(ids))-1000, np.round((ids-np.int_(np.floor(ids)))*1e4) ) )
                trace.stats.IDs['idvr'] = ( "%.2i.%.5i.00" % ( np.int_(np.floor(idr))-1000, np.round((idr-np.int_(np.floor(idr)))*1e4) ) )
            except :
            # add generic src/rec IDs
                trace.stats.IDs['id_src'] = network
                trace.stats.IDs['id_rec'] = station

            # add lon/lat coords (in progress...)
            trace.stats.coordinates['coord_src_lonlat'] = [ None, None, None ]
            trace.stats.coordinates['coord_rec_lonlat'] = [ None, None, None ]

            #dist_m, az, baz = calc_vincenty_inverse(lat_ev, lon_ev, lat_st, lon_st, a=semi_maj, f=flattening)
            trace.stats.distances['dist_lonlat'] = None
            trace.stats.azimuths['az_lonlat'] = None
            trace.stats.azimuths['baz_lonlat'] = None

            if irec == 0 :
                # init. stream with first trace
                stream = obspy.Stream(traces=[trace])
            else:
                stream.append(trace)

            # update trace nb before next one
            itr = itr + 1
        #-- end loop over rec (irec)

        stream_list.append( stream )

    #-- end loop over src (isrc)

    return stream_list

#-- end function convert_SEM46_binary_seismo_to_Obspy_stream


# ----------------------------------------------------------------------


def azimuth_xy(x, y, mode="180") :
    '''
    Compute azimuth from Cartesian (x,y) coordinates.

    Francois Lavoue (francois.lavoue@univ-grenoble-alpes.fr), 30 July 2019  
    Copyright (c) 2019, Univ. Grenoble Alpes
    '''

    import numpy as np

    # init to Nan
    az = np.nan

    if y == 0.0 :
        if x > 0 :
            az = 90.0
        elif x < 0 :
            az = -90.0
        # (else stays nan)

    elif y >0 :
        az = 180.0*np.arctan(x/y) / np.pi

    elif y < 0 and x >= 0:
        az = 180.0*np.arctan(x/y) / np.pi + 180.0

    elif y < 0 and x <= 0:
        az = 180.0*np.arctan(x/y) / np.pi - 180.0

    # convert from -180/180 to 0/360
    if az < 0 and mode == "360" :
        az = az + 360.0

    return az

#-- end function azimuth_xy


# ----------------------------------------------------------------------


