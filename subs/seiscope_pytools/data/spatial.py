# ----------------------------------------------------------------------

# Copyright 2008-2011 SEISCOPE project, All rights reserved.  
# Copyright 2013-2020 SEISCOPEII project, All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#    *  Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    *  Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in
#       the documentation and/or other materials provided with the
#       distribution.
#    *  Neither the name of the SEISCOPE project nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# Warranty Disclaimer:  
# THIS SOFTWARE IS PROVIDED BY THE SEISCOPE PROJECT AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# SEISCOPE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Acknowledgements:  
# SEISCOPE project's codes have been developed in the framework of
# SEISCOPE and SEISCOPE 2 consortia, and we thank the sponsors of
# these projects.

# ----------------------------------------------------------------------

class Points:

    """
    A class to specify point(s) in space

    Author: Theodosius Marwan Irnaka (theodosius-marwan.irnaka@univ-grenoble-alpes.fr), 2019  

    ...

    Attributes
    ----------
    dimension : int
        dimension of your spatial points
    npoints : int
        total number of points

    Methods
    -------
    add(coordinate)
        add new set of points into the existing Points class
    """

    def __init__(self,coordinate=None):
        if coordinate==None:
            coordinate = (None,None,None)
            self.dimension = 0
        else:
            if not isinstance(coordinate,tuple):
                raise TypeError("Input must be a tuple")

            if len(coordinate)==1:
                self.X = coordinate
                self.dimension = 1
            elif len(coordinate)==2:
                self.X,self.Y = coordinate
                self.dimension = 2
            elif len(coordinate)==3:
                self.X,self.Y,self.Z = coordinate
                self.dimension = 3
            else:
                raise ValueError("Dimension >3 is not supported!")
            self.npoints = len(self.X)
        if self.dimension>0: self.X,self.Y,self.Z = list(self.X),list(self.Y),list(self.Z)

    def add(self,coordinate):
        if len(coordinate)!=self.dimension: raise IOError("Input dimension is not consistent with the existing dimension")
        if self.dimension>=1: self.X.extend(coordinate[0])
        if self.dimension>=2: self.Y.extend(coordinate[1])
        if self.dimension==3: self.Z.extend(coordinate[2])
        self.npoints = len(self.X)

    def __repr__(self):
        return f'This Point class contains {len(self.X)} and has {self.dimension} dimension.'

#-- end class Points

# ----------------------------------------------------------------------

