# ----------------------------------------------------------------------

# Copyright 2008-2011 SEISCOPE project, All rights reserved.  
# Copyright 2013-2020 SEISCOPEII project, All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#    *  Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    *  Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in
#       the documentation and/or other materials provided with the
#       distribution.
#    *  Neither the name of the SEISCOPE project nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# Warranty Disclaimer:  
# THIS SOFTWARE IS PROVIDED BY THE SEISCOPE PROJECT AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# SEISCOPE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Acknowledgements:  
# SEISCOPE project's codes have been developed in the framework of
# SEISCOPE and SEISCOPE 2 consortia, and we thank the sponsors of
# these projects.

# ----------------------------------------------------------------------

class Mesh_Param:

    """
    Class for SEM46 mesh input parameters listed in mesh_input_namelist.

    Author: Theodosius Marwan Irnaka (theodosius-marwan.irnaka@univ-grenoble-alpes.fr), 2019  
    Last updated Francois Lavoue (francois.lavoue@univ-grenoble-alpes.fr), 22 July 2019
    """

    def __init__(self,**kwargs):
        self.val2 = kwargs.get('val2',"default value")

        # MESH_INFO
        self.mesh_option = kwargs.get('mesh_option',None)
        self.IS_variable_mesh = kwargs.get('IS_variable_mesh',None)
        self.mesh_name = kwargs.get('mesh_name',None)
        self.ISvtk_mesh = kwargs.get('ISvtk_mesh',None)
        self.sponge_length = kwargs.get('sponge_length',None)

        # BOUNDARY_ABC
        self.iABC = kwargs.get('iABC',None)

        # MESH HOMO
        # constant element-size
        self.Nb_elementZ = kwargs.get('Nb_elementZ',None)
        self.Nb_elementX = kwargs.get('Nb_elementX',None)
        self.Nb_elementY = kwargs.get('Nb_elementY',None)
        self.Nb_deformed_element = kwargs.get('Nb_deformed_element',None)
        self.dZ = kwargs.get('dZ',None)
        self.dX = kwargs.get('dX',None)
        self.dY = kwargs.get('dY',None)

        # variable element-size
        self.sizeZ = kwargs.get('sizeZ',None)
        self.sizeX = kwargs.get('sizeX',None)
        self.sizeY = kwargs.get('sizeY',None)
        self.IS_predesigned_mesh = kwargs.get('IS_predesigned_mesh',None)
        self.deformation_ratio = kwargs.get('deformation_ratio',None)
        self.inter_element_size_name =  kwargs.get('inter_element_size_name',None)
        self.extra_mesh_info = kwargs.get('extra_mesh_info',None)

        # automatic mesh
        self.nb_points_per_shortest_wavelength = kwargs.get('nb_points_per_shortest_wavelength',None)
        self.frequency_max = kwargs.get('frequency_max',None)

        # predesigned mesh
        self.input_element_size_name = kwargs.get('input_element_size_name',None)

        self.maxV_homo = kwargs.get('maxV_homo',None)
        self.minV_homo = kwargs.get('minV_homo',None)
        self.maxV_name = kwargs.get('maxV_name',None)
        self.minV_name = kwargs.get('minV_name',None)
        self.Nb_point_Z = kwargs.get('Nb_point_Z',None)
        self.Nb_point_X = kwargs.get('Nb_point_X',None)
        self.Nb_point_Y = kwargs.get('Nb_point_Y',None)
        self.dZ_reg = kwargs.get('dZ_reg',None)
        self.dX_reg = kwargs.get('dX_reg',None)
        self.dY_reg = kwargs.get('dY_reg',None)
        self.x_origin = kwargs.get('x_origin',None)
        self.y_origin = kwargs.get('y_origin',None)
        self.z_origin = kwargs.get('z_origin',None)

        # TOPOGRAPHY
        self.topo_name = kwargs.get('topo_name',None)
        self.topo_Nb_point_X = kwargs.get('topo_Nb_point_X',None)
        self.topo_Nb_point_Y = kwargs.get('topo_Nb_point_Y',None)
        self.topo_dX = kwargs.get('topo_dX',None)
        self.topo_dY = kwargs.get('topo_dY',None)
        self.topo_x_origin = kwargs.get('topo_x_origin',None)
        self.topo_y_origin = kwargs.get('topo_y_origin',None)
        self.topo_z_origin = kwargs.get('topo_z_origin',None)

#-- end class Mesh_Param

# ----------------------------------------------------------------------

class Projection_Param:

    """
    Class for SEM46 projection parameters listed in projection_input_namelist.

    Author: Theodosius Marwan Irnaka (theodosius-marwan.irnaka@univ-grenoble-alpes.fr), 2019  
    Last updated Francois Lavoue (francois.lavoue@univ-grenoble-alpes.fr), 22 July 2019
    """

    def __init__(self,**kwargs):

        # working option
        self.working_option = kwargs.get('working_option',None)
        # self.number_file = kwargs.get('number_file',2)
        self.fast_projection = kwargs.get('fast_projection','.None.')
        self.name_file_cart = kwargs.get('name_file_cart',(None,None))
        self.name_file_gll = kwargs.get('name_file_gll',(None,None))
        self.number_file = None   #len(self.name_file_cart)

        #if self.working_option==1:
        self.Nb_point_Z = kwargs.get('Nb_point_Z',None)
        self.Nb_point_X = kwargs.get('Nb_point_X',None)
        self.Nb_point_Y = kwargs.get('Nb_point_Y',None)
        self.dZ = kwargs.get('dZ',None)
        self.dX = kwargs.get('dX',None)
        self.dY = kwargs.get('dY',None)
        self.x_origin = kwargs.get('x_origin',None)
        self.y_origin = kwargs.get('y_origin',None)
        self.z_origin = kwargs.get('z_origin',None)
        self.ISvtk = kwargs.get('ISvtk',None)

        #if self.working_option==2:
        #self.dZ = kwargs.get('dZ',10.)
        #self.dX = kwargs.get('dX',10.)
        #self.dY = kwargs.get('dY',10.)
        self.Ylinepos = kwargs.get('Ylinepos',None)

        self.acqui_in = kwargs.get('acqui_in',None)
        self.acqui_out = kwargs.get('acqui_out',None)
        self.acqui_x_origin = kwargs.get('acqui_x_origin',None)
        self.acqui_y_origin = kwargs.get('acqui_y_origin',None)

#-- end class Projection_Param

# ----------------------------------------------------------------------

class Sem_Param:

    """
    Class for SEM46 parameters listed in sem_input_namelist.

    Author: Theodosius Marwan Irnaka (theodosius-marwan.irnaka@univ-grenoble-alpes.fr), 2019  
    Last updated Francois Lavoue (francois.lavoue@univ-grenoble-alpes.fr), 22 July 2019
    """

    def __init__(self,**kwargs):
        # WORKING MODE
        self.working_mode = kwargs.get('working_mode',None)

        # ACQUISITION INFORMATION
        self.acqui_type = kwargs.get('acqui_type',None)
        self.x_origin = kwargs.get('x_origin',None)
        self.y_origin = kwargs.get('y_origin',None)
        self.acqui_file = kwargs.get('acqui_file',None)
        self.ISshot_by_shot = kwargs.get('ISshot_by_shot',None)
        self.ISsame_receiver = kwargs.get('ISsame_receiver',None)
        
        # SOURCE INFORMATION
        self.source_file_name = kwargs.get('source_file_name',None)
        self.nb_sample = kwargs.get('nb_sample',None)
        self.sourcepar = kwargs.get('sourcepar',None)

        self.ISdifferent = kwargs.get('ISdifferent',None)
        self.ISFz = kwargs.get('ISFz',None)
        self.ISFx = kwargs.get('ISFx',None)
        self.ISFy = kwargs.get('ISFy',None)
        self.M_zz = kwargs.get('M_zz',None)
        self.M_xx = kwargs.get('M_xx',None)
        self.M_yy = kwargs.get('M_yy',None)
        self.M_xz = kwargs.get('M_xz',None)
        self.M_xy = kwargs.get('M_xy',None)
        self.M_yz = kwargs.get('M_yz',None)
        self.source_type_file = kwargs.get('source_type_file',None)
        self.n_source = kwargs.get('n_sources',None)

        # DOMAIN DESCRIPTION
        self.Nb_domainZ = kwargs.get('Nb_domainZ',None)
        self.Nb_domainX = kwargs.get('Nb_domainX',None)
        self.Nb_domainY = kwargs.get('Nb_domainY',None)

        # PHYSICAL PARAMETER
        self.vp_homo  = kwargs.get('Vp_homo',None)
        self.vs_homo  = kwargs.get('Vs_homo',None)
        self.den_homo = kwargs.get('Den_homo',None)
        self.vp_name  = kwargs.get('Vp_name',None)
        self.vs_name  = kwargs.get('Vs_name',None)
        self.den_name = kwargs.get('Den_name',None)

        # SIMULATION INFO
        self.dt = kwargs.get('dt',None)
        self.total_nb_time_step = kwargs.get('total_nb_time_step',None)
        self.ISseisZ = kwargs.get('ISseisZ',None)
        self.ISseisX = kwargs.get('ISseisX',None)
        self.ISseisY = kwargs.get('ISseisY',None)
        self.name_seis = kwargs.get('name_seis',None)

        # SNAPSHOP IN FORWARD MODELING
        self.ISnap = kwargs.get('Isnap',None)
        self.snap_name_Z = kwargs.get('snap_name_Z',None)
        self.snap_name_X = kwargs.get('snap_name_X',None)
        self.snap_name_Y = kwargs.get('snap_name_Y',None)
        self.direction = kwargs.get('direction',None)
        self.step = kwargs.get('step',None)

        # ATTENUATION
        self.min_freq = kwargs.get('min_freq',None)
        self.max_freq = kwargs.get('max_freq',None)
        self.Q_ref = kwargs.get('Q_ref',None)
        self.Qphomo = kwargs.get('Qphomo',None)
        self.Qshomo = kwargs.get('Qshomo',None)
        self.Qp_file = kwargs.get('Qp_file',None)
        self.Qs_file = kwargs.get('Qs_file',None)

#-- end class Sem_Param

# ----------------------------------------------------------------------

