#
# Adjust length of signals and spectra to time and frequency vectors.
#
# Francois Lavoue (francois.lavoue@univ-grenoble-alpes.fr), 30 June 2020
# Copyright (c) 2020, Univ. Grenoble Alpes

import numpy as np

def adjust_signal_length(v_in, f_in) :

    if abs( len(v_in) - len(f_in) ) > 1 :
       print("Error: the difference in length of v_in and f_in should be at most 1.")
       print("       Here, len(v_in) = %i and len(f_in) = %i." % (len(v_in),len(f_in)))
       exit("Error: the difference in length of v_in and f_in should be at most 1.")

    n_in = len(f_in)
    n_ou = len(v_in)

    if n_in == n_ou :
       f_ou = f_in
    elif n_in == n_ou + 1 :
       f_ou = f_in[0:n_ou]
    elif n_in == n_ou - 1 :
       f_ou = np.concatenate( ( np.reshape(f_in, (n_in)), np.reshape(f_in[-1],(1)) ) )
       #f_ou = np.array( list(f_in).append(f_in[-1]) )
    else :
       print("Error (2): the difference in length of v_in and f_in should be at most 1.")
       print("           Here, len(v_in) = %i and len(f_in) = %i." % (len(v_in),len(f_in)))
       exit("Error (2): the difference in length of v_in and f_in should be at most 1.")

    return f_ou

#-- end def [f_ou] = adjust_signal_length(v_in, f_in)

