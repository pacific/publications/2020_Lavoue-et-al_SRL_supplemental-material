#
# Plot aliases of a vector of frequencies vs. time.
#
# Francois Lavoue (francois.lavoue@univ-grenoble-alpes.fr), 10 June 2020
# Copyright (c) 2020, Univ. Grenoble Alpes

def plot_aliases(vt, f_vs_time, dt, ax=None) :
    # plot initial vector if it fits in the frequency range of the FFT (Nyquist criterion)
    # (disabled because already done in main script)
    #if f_vs_time[-1] < 0.5/dt :
    #    ax.plot(vt, f_vs_time, "--", dashes=(6,6), color="C3", linewidth=1.5)
    #else :
        # 1st alias: mirror wrt. fmax = 0.5/dt (f_alias1 = 0.5/dt - (f_vs_time-0.5/dt) = 1/dt - f_vs_time)
        f_alias1 = 1/dt - f_vs_time
        if f_alias1[-1] > 0.0 :   # plot 1st alias
            ax.plot(vt, f_alias1, "--", dashes=(5,5), color="C1", linewidth=1.25)
        else :   # 2nd alias: mirror wrt. 0
            f_alias2 = -f_alias1
            if f_alias1.max() > 0.0 :    # still plot 1st alias if part of it is between 0 and fmax
                ax.plot(vt, f_alias1, "--", dashes=(5,5), color="C1", linewidth=1.25)
            if f_alias2[-1] < 0.5/dt :   # plot 2nd alias
                ax.plot(vt, f_alias2, "--", dashes=(4,4), color="C8", linewidth=1.25)
            else :   # 3rd alias: mirror wrt. fmax
                f_alias3 = 1/dt - f_alias2
                if f_alias2.min() < 0.5/dt :
                    ax.plot(vt, f_alias2, "--", dashes=(4,4), color="C8", linewidth=1.25)
                if f_alias3[-1] > 0.0 :
                    ax.plot(vt, f_alias3, "--", dashes=(3,3), color="C9", linewidth=1.25)
                else :   # etc ...
                    f_alias4 = -f_alias3
                    if f_alias3.max() > 0.0 :
                        ax.plot(vt, f_alias3, "--", dashes=(3,3), color="C9", linewidth=1.25)
                    if f_alias4[-1] < 0.5/dt :
                        ax.plot(vt, f_alias4, "--", dashes=(3,3), color="C4", linewidth=1.25)
                    else :
                        f_alias5 = 1/dt - f_alias4
                        if f_alias4.min() < 0.5/dt :
                            ax.plot(vt, f_alias4, "--", dashes=(3,3), color="C4", linewidth=1.25)
                        if f_alias5[-1] > 0.0 :
                            ax.plot(vt, f_alias5, "--", dashes=(3,3), color="C0", linewidth=1.25)
                        else :
                            f_alias6 = -f_alias5
                            if f_alias5.max() > 0.0 :
                                ax.plot(vt, f_alias5, "--", dashes=(3,3), color="C0", linewidth=1.25)
                            if f_alias6[-1] < 0.5/dt :
                                ax.plot(vt, f_alias6, "--", dashes=(3,3), color="C5", linewidth=1.25)

#-- end def plot_aliases()

