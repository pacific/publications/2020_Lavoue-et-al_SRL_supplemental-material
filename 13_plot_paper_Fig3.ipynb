{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Plot paper Figure 3\n",
    "\n",
    "### (track deflection due to spatial load distribution and related spectrum)\n",
    "\n",
    "François Lavoué (francois.lavoue@univ-grenoble-alpes.fr), 31 July 2019  \n",
    "Last updated François Lavoué, 1 May 2020  \n",
    "Copyright (c) 2019-2020, Univ. Grenoble Alpes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "## Contents\n",
    "\n",
    "- Notes\n",
    "\n",
    "- Modules\n",
    "\n",
    "- User parameters\n",
    "\n",
    "- Account for rail track elasticity and distributed wheel load  \n",
    "    - Triangular/Gaussian loading weights after Paderno (2009)  \n",
    "    - Elastic reaction force (E-BEB model)\n",
    "\n",
    "- Plot Figure 3: loading weight functions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "## Notes\n",
    "\n",
    "- This work is licensed under the BSD 3-Clause License (see `LICENSE` file).\n",
    "\n",
    "- This notebook can be converted to a Python script using\n",
    "  ```\n",
    "  jupyter nbconvert --to script 13_plot_paper_Fig3.ipynb\n",
    "  ```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "## Modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Backward compatibility of print() between Python 2 and 3\n",
    "#from __future__ import print_function\n",
    "\n",
    "# Plot figures directly in the notebook\n",
    "%matplotlib notebook\n",
    "\n",
    "# Plot defaults\n",
    "import matplotlib.pyplot as plt\n",
    "plt.style.use('seaborn-talk')                 # Matplotlib style sheet\n",
    "plt.rcParams['figure.figsize'] = 10,5\n",
    "\n",
    "# Font sizes\n",
    "SMALL_SIZE = 10\n",
    "MEDIUM_SIZE = 12\n",
    "BIGGER_SIZE = 14\n",
    "plt.rc('font', size=SMALL_SIZE)          # controls default text sizes\n",
    "plt.rc('axes', titlesize=MEDIUM_SIZE)     # fontsize of the axes title\n",
    "plt.rc('axes', labelsize=SMALL_SIZE)     # fontsize of the x and y labels\n",
    "plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels\n",
    "plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels\n",
    "plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize\n",
    "plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%javascript\n",
    "// Disable Auto-scrolling\n",
    "IPython.OutputArea.prototype._should_scroll = function(lines){return false;}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#-- import general-purpose modules\n",
    "import numpy as np\n",
    "import obspy\n",
    "import os"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#-- import modules specific to this work\n",
    "from scipy import signal\n",
    "from scipy import interpolate"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "## User parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dir_ou = \"figs/figs_paper\"\n",
    "\n",
    "file_ou = dir_ou + \"/Fig3_distributed_loading_weights_windows\"\n",
    "\n",
    "if not os.path.isdir(dir_ou): os.makedirs(dir_ou)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#-- time and space parameters\n",
    "\n",
    "dt = 0.004\n",
    "dx = 0.01"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#-- train parameters (v1: simple case with one wheel per wagon)\n",
    "\n",
    "case = \"Austrian high-speed train\"   # \"Canadian_freight_train\" or \"Austrian high-speed train\"\n",
    "\n",
    "if case == \"Austrian high-speed train\" :\n",
    "    # Austrian Railjet high-speed train (Fuchs et al., 2018, Table 1)\n",
    "    V_train = 33.125  # train speed (in m/s, 13.889 m/s -> 50 km/h, 19.444 m/s -> 70 km/h, 23.611 m/s -> 85 km/h, 25 m/s -> 90 km/h, 33.125 m/s -> 119.25 km/h ==> f1 = 1.25 Hz)\n",
    "    L_w = 26.5   # wagon length (in m)\n",
    "                 # 26.5 m -> Austrian high-speed train\n",
    "                 # 19 m -> bogie distance\n",
    "    N_w = 8      # nb of wagons\n",
    "    weight_on_axle_kg = 17e3\n",
    "    \n",
    "elif case == \"Canadian freight train\" :\n",
    "    # Canadian National, 50' metals boxcar\n",
    "    # (https://www.cn.ca/-/media/Files/Our-Business/Equipment/MM-metals-boxcar-en.pdf?la=en&hash=A1AFD18B4BC545A2A5F1BA69287ADB75520B48CD)\n",
    "    V_train = 13.889   # train speed (in m/s, 13.889 m/s -> 50 km/h, 19.444 m/s -> 70 km/h, 23.611 m/s -> 85 km/h, 25 m/s -> 90 km/h)\n",
    "    L_w = 16.9164      # wagon length (in m, 16.9164 m = 55 ft 6 in)\n",
    "    N_w = 59           # nb of wagons\n",
    "    weight_on_axle_kg = 17e3   # (same as high-speed train for comparison)\n",
    "\n",
    "nw_pw = 4    # nb of wheels per wagon\n",
    "\n",
    "DX = 0.6096   # sleeper spacing (in m, 0.6096 m = 24 in)\n",
    "\n",
    "L_train = N_w*L_w\n",
    "weight_on_axle_N = weight_on_axle_kg * 9.81\n",
    "\n",
    "print(\"%s :\" % case)\n",
    "print(\"   %ix %g-m-long wagons\" % (N_w, L_w))\n",
    "print(\"   total length = %g m\" % (L_train))\n",
    "print(\"   axle load = %g tons (%g kN)\" % (weight_on_axle_kg/1e3           , weight_on_axle_N/1e3))\n",
    "print(\"   total load = %g tons (%g kN)\" % (nw_pw*N_w*weight_on_axle_kg/1e3, nw_pw*N_w*weight_on_axle_N/1e3))\n",
    "print(\"   train speed = %g m/s   (%.2f km/h)\" % (V_train, V_train*3.6))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#-- more train parameters (v2: 4 wheels per wagons)\n",
    "\n",
    "if \"Austrian high-speed train\" :\n",
    "    # Austrian Railjet high-speed train (Fuchs et al., 2018, Table 1)\n",
    "    dw1 = 2.5    # distance between closest wheels (m), i.e. between axles within 1 bogie\n",
    "    db1 = 19.0   # distance between bogies within same wagon (m)\n",
    "    db2 = 7.5    # distance between bogies of 2 consecutive wagons (m)\n",
    "    # position of 1st wheel (m)\n",
    "    xw1 = 0.5 * ( L_w - db1 - dw1 )   # this should result in x_inter_wagons = 0\n",
    "    #xw1 = 2.0   # (bad idea, because then x_inter_wagons =/= 0)\n",
    "    # distance between wagons (m)\n",
    "    x_inter_wagons = db2 - 2*xw1 - dw1\n",
    "\n",
    "elif case == \"Canadian freight train\" :\n",
    "    # Canadian National, 50' metals boxcar\n",
    "    #https://www.cn.ca/-/media/Files/Our-Business/Equipment/MM-metals-boxcar-en.pdf?la=en&hash=A1AFD18B4BC545A2A5F1BA69287ADB75520B48CD\n",
    "    dw1 = 1.654     # distance between closest wheels (in m), i.e. between axles within 1 bogie\n",
    "    db1 = 12.4841   # distance between bogies within same wagon (in m, 12.4841 m = 40 ft 11.5 in)\n",
    "    #db2 =          # distance between bogies of 2 consecutive wagons (m)\n",
    "    x_inter_wagons = 0.0211   # distance between wagons (m) [NB: this value is chosen to accommodate rounding/measuring errors, such that we end up with 0 checks below]\n",
    "    xw1 = 1.3786 + 0.5*x_inter_wagons    # position of 1st wheel within wagon (m)\n",
    "    x_inter_wagons = 0.0      # (now absorbed in xw1)\n",
    "    db2 = 2*xw1 + x_inter_wagons + dw1   # distance between bogies of 2 consecutive wagons (m)\n",
    "\n",
    "print(case)\n",
    "print(\"   wagon length:   L_w = %g m\" % L_w)\n",
    "print(\"   position of 1st wheel within wagon:   xw1 = %g m\" % xw1)\n",
    "print(\"   distance between axles within 1 bogie:   dw1 = %g m\" % dw1)\n",
    "print(\"   distance between bogies within same wagon:   db1 = %g m\" % db1)\n",
    "print(\"   distance between bogies of 2 consecutive wagons:   db2 = %g m\" % db2)\n",
    "print(\"   distance between wagons = %g m\" % x_inter_wagons)\n",
    "\n",
    "print(\"\\nChecks:\")\n",
    "print(\"   x_inter_wagons + 2*xw1 + dw1 + db1 - L_w = %g m   (should be 0)\" % (x_inter_wagons + 2*xw1 + dw1 + db1 - L_w))\n",
    "print(\"   db1 + db2 - L_w = %g m   (should be 0)\" % (db1 + db2 - L_w))\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#-- more parameters...\n",
    "# (sometimes the same but with other notations...)\n",
    "\n",
    "dx_src = DX       # sleeper spacing\n",
    "dx_rec = DX\n",
    "dx_wagon = L_w    # wagon length\n",
    "dx_bogie1 = db1   # distance between bogies within same wagon\n",
    "dx_bogie2 = db2   # distance between bogies of 2 consecutive wagons\n",
    "dx_axle = dw1     # distance between closest wheels, i.e. between axles within 1 bogie\n",
    "\n",
    "#-- decides when and where the train starts moving\n",
    "tmin_tot = 0.0\n",
    "t0_train = 1.0    # time at which the train starts (in s, to avoid putting a Dirac at t/x = 0)\n",
    "\n",
    "dt_acc = 2*60.0   # duration of each acceleration/decceleration phase (in s, i.e. 2 min)\n",
    "n_acc = 2         # nb of acceleration/decceleration phases (including a stop in the middle)\n",
    "# NB: should be 2 if we build the speed vector by duplication below,\n",
    "#     should be 4 if we builf it with 2 acceleration/decceleration phases"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#-- time and space discretization (source time functions)\n",
    "\n",
    "n_ext = 100\n",
    "\n",
    "vx_load = np.arange(-n_ext*dx_rec,         n_ext*dx_rec+0.1*dx,         dx)\n",
    "vt_load = np.arange(-n_ext*dx_rec/V_train, n_ext*dx_rec/V_train+0.1*dt, dt)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#-- time and space discretization (full vectors)\n",
    "\n",
    "L_tot = 12e3   # rail length (in m)\n",
    "\n",
    "vt_tot = np.arange(0.0, L_tot/V_train, dt)\n",
    "\n",
    "vt = vt_tot\n",
    "nt = len(vt)\n",
    "\n",
    "print(\"tmax = %g s\" % (vt.max()))\n",
    "print(\"nt = %i\" % len(vt))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#-- define simple Dirac in space and time\n",
    "\n",
    "maxw_load = 0.39   # FL: probably we shouldn't normalize wrt Paderno's weights anymore... should we?\n",
    "#maxw_load = 1.0\n",
    "\n",
    "vwx_load_dirac = signal.unit_impulse(len(vx_load), np.int_(np.round(0.5*len(vx_load)))) * maxw_load\n",
    "vwt_load_dirac = signal.unit_impulse(len(vt_load), np.int_(np.round(0.5*len(vt_load)))) * maxw_load"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "## Compute elastic reaction force (E-BEB model)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def f_Pj_xi(params, xi):\n",
    "    \n",
    "    # input parameters\n",
    "    Fi = params[0]      # wheel load (in N?)\n",
    "    alpha = params[1]   # ground stiffness (in N/m^2)\n",
    "    #xi = vx_load       # distance between wheel i and sleeper j (in m)\n",
    "    \n",
    "    # fixed parameters\n",
    "    #E = 2.06e11    # elastic (Young) modulus of the rail (N/m^2)\n",
    "    #I = ?          # cross-sectional momentum of the rail\n",
    "    EI = 58.5e6     # E*I value of the rail (in N.m^2)\n",
    "    beta = (alpha/(4*EI))**0.25\n",
    "    x0 = np.pi/beta\n",
    "\n",
    "    #dd = 0.1524   # sleeper width (in m, 0.1524 m = 6 in)\n",
    "    dd = dx_rec    # sleeper spacing (in m)\n",
    "\n",
    "    #print(\"beta = %g   (should be 0.455 according to Li et al. (2018)\" % beta)\n",
    "\n",
    "    wj_xi = (0.5*Fi*beta/alpha) * np.exp(-beta*abs(xi)) * ( np.cos(beta*xi) + np.sin(beta*abs(xi)) )\n",
    "    Pj_xi =    2*Fi             * np.exp(-beta*abs(xi)) * ( np.cos(beta*xi) + np.sin(beta*abs(xi)) ) * dd / x0\n",
    "\n",
    "    return Pj_xi"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "## Account for rail track elasticity and distributed wheel load"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### -1- Triangular/Gaussian loading weights after Paderno (2009)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#-- define wheel load on neighbouring sleepers/crossties (after Paderno, 2009)\n",
    "x_load = [ -2*dx_rec, -1*dx_rec, 0.0, dx_rec, 2*dx_rec ]\n",
    "w_load = [ 0.07, 0.235, 0.39, 0.235, 0.07 ]\n",
    "\n",
    "# convert to arrays\n",
    "x_load = np.array(x_load)\n",
    "w_load = np.array(w_load)\n",
    "\n",
    "# convert x to time (assuming constant train speed)\n",
    "t_load = x_load/V_train\n",
    "\n",
    "# regrid in space and time\n",
    "from scipy import interpolate\n",
    "finterp_x = interpolate.interp1d(x_load, w_load, fill_value=\"extrapolate\")\n",
    "finterp_t = interpolate.interp1d(t_load, w_load, fill_value=\"extrapolate\")\n",
    "\n",
    "vx1_load = np.arange(x_load.min(), x_load.max()+0.1*dx, dx)\n",
    "vt1_load = np.arange(t_load.min(), t_load.max()+0.1*dt, dt)\n",
    "vx2_load = np.arange(-n_ext*dx_rec,         n_ext*dx_rec+0.1*dx,         dx)\n",
    "vt2_load = np.arange(-n_ext*dx_rec/V_train, n_ext*dx_rec/V_train+0.1*dt, dt)\n",
    "\n",
    "# interpolate\n",
    "vwx1_load = finterp_x(vx1_load)\n",
    "vwt1_load = finterp_t(vt1_load)\n",
    "vwx2_load = finterp_x(vx2_load)\n",
    "vwt2_load = finterp_t(vt2_load)\n",
    "\n",
    "# correct negative values\n",
    "vwx1_load[vwx1_load<0] = 0.0\n",
    "vwt1_load[vwt1_load<0] = 0.0\n",
    "vwx2_load[vwx2_load<0] = 0.0\n",
    "vwt2_load[vwt2_load<0] = 0.0\n",
    "\n",
    "print(\"Reminder: dx_rec =\", dx_rec)\n",
    "print(\"          x_load =\", x_load)\n",
    "print(\"          w_load =\", w_load)\n",
    "\n",
    "#print(sum(w_load))\n",
    "print(\"\\nfurther checks:\")\n",
    "print(vt_tot[0:4])\n",
    "print(vt1_load[0:4])\n",
    "print(t_load.min(), t_load.max())\n",
    "print(vt1_load.min(), vt1_load.max())\n",
    "#print(vt2_weights.min(), vt2_weights.max())\n",
    "#print(vwg_weights.min(), vwg_weights.max())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#-- rename output\n",
    "vt_load = vt2_load\n",
    "vx_load = vx2_load\n",
    "vwx_load_trian = vwx2_load\n",
    "vwt_load_trian = vwt2_load\n",
    "#vwt_load_gauss = vwt2_load\n",
    "#vwt_load_elast = vwt2_load\n",
    "\n",
    "del vt2_load, vx2_load, vwx2_load, vwt2_load"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#-- interpolate as a Gaussian\n",
    "from scipy.optimize import least_squares\n",
    "\n",
    "def gaussian(params, x):\n",
    "    A0 = params[0]\n",
    "    mu = params[1]\n",
    "    sigma = params[2]\n",
    "    return A0 * np.exp( - ( x - mu )**2 / sigma**2 )\n",
    "\n",
    "#fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(10,5))\n",
    "#ax.plot(vt_load, gaussian([1,0,0.02], vt_load))\n",
    "\n",
    "def gaussian_residuals(params, x, y):\n",
    "    return gaussian(params,x) - y\n",
    "\n",
    "#-- find optimal space-domain Gaussian\n",
    "par_ini_x = [ w_load.max(), 0.0, 0.5*dx_rec ]\n",
    "par_opt_x = least_squares(gaussian_residuals, par_ini_x, args=(x_load, w_load))\n",
    "#res_robust = least_squares(fun, x0, loss='soft_l1', f_scale=0.1, args=(t_train, y_train))\n",
    "\n",
    "# define Gaussian fitting weights\n",
    "vwx_load_gauss = gaussian(par_opt_x.x, vx_load)\n",
    "\n",
    "#-- find optimal time-domain Gaussian\n",
    "par_ini_t = [ w_load.max(), 0.0, 0.5*dx_rec/V_train ]\n",
    "par_opt_t = least_squares(gaussian_residuals, par_ini_t, args=(t_load, w_load))\n",
    "#res_robust = least_squares(fun, x0, loss='soft_l1', f_scale=0.1, args=(t_train, y_train))\n",
    "\n",
    "# define Gaussian fitting weights\n",
    "vwt_load_gauss = gaussian(par_opt_t.x, vt_load)\n",
    "\n",
    "print(\"par_opt_x =\")\n",
    "print(\"opt. A = %g, mu = %g m, sigma = %g m\" % (par_opt_x.x[0], par_opt_x.x[1], par_opt_x.x[2]))\n",
    "#print(par_opt_x)\n",
    "print()\n",
    "print(\"par_opt_t =\")\n",
    "print(\"opt. A = %g, mu = %g s, sigma = %g s\" % (par_opt_t.x[0], par_opt_t.x[1], par_opt_t.x[2]))\n",
    "#print(par_opt_t)\n",
    "\n",
    "print(\"\\nReminder: dx_rec =\", dx_rec)\n",
    "print(\"          x_load =\", x_load)\n",
    "print(\"          w_load =\", w_load)\n",
    "print(\"          sigma_x = %g m\" % par_opt_x.x[2])\n",
    "print(\"        5*sigma_x = %g m\" % (5*par_opt_x.x[2]))\n",
    "print(\"          sigma_t = %g s   (for V_train = %g m/s)\" % (par_opt_t.x[2], V_train))\n",
    "print(\"        5*sigma_t = %g s\" % (5*par_opt_t.x[2]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "### Plot Fig. 3: loading weight functions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#-- plot loading weights\n",
    "import matplotlib.ticker as mticker\n",
    "from matplotlib.ticker import FormatStrFormatter\n",
    "\n",
    "#-- define loading weight function for different elastic weights\n",
    "Fi = 1.0\n",
    "\n",
    "alpha1 = 8e8   # optimal value matching Paderno's weights\n",
    "params = [ Fi, alpha1 ]\n",
    "vwx_load_elast1 = f_Pj_xi(params, vx_load)\n",
    "vwx_load_elast1 = max(abs(w_load))*vwx_load_elast1/max(abs(vwx_load_elast1))\n",
    "\n",
    "alpha2 = 61.8e6   # value from Krylov and Ferguson (1994) for British Rails\n",
    "params = [ Fi, alpha2 ]\n",
    "vwx_load_elast2 = f_Pj_xi(params, vx_load)\n",
    "vwx_load_elast2 = max(abs(w_load))*vwx_load_elast2/max(abs(vwx_load_elast2))\n",
    "\n",
    "alpha3 = 10e6   # value from Li et al. (2018)\n",
    "params = [ Fi, alpha3 ]\n",
    "vwx_load_elast3 = f_Pj_xi(params, vx_load)\n",
    "vwx_load_elast3 = max(abs(w_load))*vwx_load_elast3/max(abs(vwx_load_elast3))\n",
    "\n",
    "#-- convert from space to time\n",
    "ti = vx_load / V_train\n",
    "finterp1 = interpolate.interp1d(ti, vwx_load_elast1, fill_value=\"extrapolate\")\n",
    "vwt_load_elast1 = finterp1(vt_load)\n",
    "finterp2 = interpolate.interp1d(ti, vwx_load_elast2, fill_value=\"extrapolate\")\n",
    "vwt_load_elast2 = finterp2(vt_load)\n",
    "finterp3 = interpolate.interp1d(ti, vwx_load_elast3, fill_value=\"extrapolate\")\n",
    "vwt_load_elast3 = finterp3(vt_load)\n",
    "\n",
    "#-- compute FFT\n",
    "fmax = 0.5/dt\n",
    "df = 1/(max(vt_load)-min(vt_load))\n",
    "vf = np.arange(0.0, fmax+0.1*df, df)   # NB: sometimes we need to add or subtract one sample...\n",
    "fft_dirac = np.fft.rfft(vwt_load_dirac)\n",
    "fft_trian = np.fft.rfft(vwt_load_trian)\n",
    "fft_gauss = np.fft.rfft(vwt_load_gauss)\n",
    "fft_elast1 = np.fft.rfft(vwt_load_elast1)\n",
    "fft_elast2 = np.fft.rfft(vwt_load_elast2)\n",
    "fft_elast3 = np.fft.rfft(vwt_load_elast3)\n",
    "#fft_blakm = np.fft.rfft(vwt_load_blakm)\n",
    "#fft_blakm2 = np.fft.rfft(vwt_load_blakm2)\n",
    "\n",
    "## normalize FFT\n",
    "#for fft in [fft_dirac, fft_trian, fft_gauss, fft_elast, fft_elast2, fft_blakm, fft_blakm2] :\n",
    "#    fft = fft / max(abs(fft))\n",
    "    \n",
    "\n",
    "#-- plot\n",
    "f = mticker.ScalarFormatter(useOffset=False, useMathText=True)\n",
    "g = lambda x,pos : \"${}$\".format(f._formatSciNotation('%1.2e' % x))\n",
    "fmt = mticker.FuncFormatter(g)\n",
    "\n",
    "label0 = \"loading weights from Paderno (2009)\"\n",
    "#label1 = \"triangular window using the loading weights from Paderno (2009)\"\n",
    "#label2 = \"Gaussian fitting the loading weights from Paderno (2009)\"\n",
    "label1 = \"triangular window fitting the weights from Paderno (2009)\"\n",
    "label2 = \"Gaussian fitting the weights from Paderno (2009)\"\n",
    "#label31 = \"$P_j(x_i)$ after Li et al. (2018), eq. (3), with $\\\\alpha = ${}\".format(fmt(alpha1)) + \" N.m$^2$\"\n",
    "#label32 = \"$P_j(x_i)$ after Li et al. (2018), eq. (3), with $\\\\alpha = ${}\".format(fmt(alpha2)) + \" N.m$^2$\"\n",
    "#label33 = \"$P_j(x_i)$ after Li et al. (2018), eq. (3), with $\\\\alpha = ${}\".format(fmt(alpha3)) + \" N.m$^2$\"\n",
    "label31 = \"E-BEB model using $\\\\alpha = ${}\".format(fmt(alpha1)) + \" N.m$^2$\"\n",
    "label32 = \"E-BEB model using $\\\\alpha = ${}\".format(fmt(alpha2)) + \" N.m$^2$\"\n",
    "label33 = \"E-BEB model using $\\\\alpha = ${}\".format(fmt(alpha3)) + \" N.m$^2$\"\n",
    "#label4 = (\"Blackman window with B = %g m\" % MM)\n",
    "#label44 = (\"Blackman window with B = %g m\" % M0)\n",
    "#label=\"val = {}\".format(fmt(alpha))\n",
    "\n",
    "nrows = 2\n",
    "fig, ax = plt.subplots(nrows=nrows, ncols=1, figsize=(10,3*nrows))\n",
    "#fig, ax = plt.subplots(nrows=nrows, ncols=1, figsize=(10,3.5*nrows))\n",
    "\n",
    "\n",
    "#-- plot f(x)\n",
    "iax = 0\n",
    "#ax[iax].plot(vx_load, vwx_load, linewidth=1)\n",
    "ax[iax].plot(vx_load, vwx_load_dirac , \"-\"  , linewidth=1.2, color=\"C0\", label=\"Dirac\")\n",
    "ax[iax].plot( x_load,   w_load       , \"o--\", linewidth=0.0, color=\"k\" , label=label0, zorder=10, markersize=8)\n",
    "ax[iax].plot(vx_load, vwx_load_trian , \"-\"  , linewidth=1.2, color=\"k\" , label=label1)\n",
    "ax[iax].plot(vx_load, vwx_load_gauss , \"--\" , linewidth=1.4, color=\"C2\", label=label2, zorder=9, dashes=(5,5))\n",
    "ax[iax].plot(vx_load, vwx_load_elast1, \"-\"  , linewidth=1.75, color=\"C3\", label=label31, zorder=8)\n",
    "ax[iax].plot(vx_load, vwx_load_elast2,\"-\"   , linewidth=1.2, color=\"C1\", label=label32)   #, dashes=(1,1))\n",
    "ax[iax].plot(vx_load, vwx_load_elast3,\"-\"   , linewidth=1.2, color=\"C8\", label=label33)   #, dashes=(1,1))\n",
    "#ax[iax].plot(vx_load, vwx_load_blakm, \"--\", linewidth=1, color=\"C4\", label=label4)\n",
    "#ax[iax].plot(vx_load, vwx_load_blakm2,\"--\", linewidth=1, color=\"C4\", label=label44, dashes=(1,1))\n",
    "ax[iax].set_xlabel(\"distance from central sleeper (m)\")\n",
    "ax[iax].set_xlim(-20*dx_rec,10*dx_rec)\n",
    "ax[iax].legend()\n",
    "\n",
    "\n",
    "# 2nd axis\n",
    "ax1 = ax[iax]\n",
    "ax2 = ax1.twiny()\n",
    "#ax2.set_xticks(new_tick_locations)\n",
    "#ax2.set_xticks(ax[iax].get_xticks())\n",
    "x_ticks = np.arange(-n_ext*dx_rec, (n_ext+1)*dx_rec, dx_rec)\n",
    "ax2.set_xticks(x_ticks)\n",
    "#ax2.set_xticklabels(np.int_(np.round(ax2.get_xticks()/dx_rec)))\n",
    "ax2.set_xticklabels([\"%i\" % x for x in np.round(ax2.get_xticks()/dx_rec)])\n",
    "#ax2.set_xticklabels([\"%i\" % x for x in x_ticks/dx_rec])\n",
    "ax2.set_xlabel(\"distance from central sleeper (unit = sleeper spacing)\")\n",
    "#ax2.xaxis.set_major_formatter(FormatStrFormatter('%.1f'))\n",
    "ax2.set_xlim(ax[iax].get_xlim())   # make sure to put this after the ticks\n",
    "\n",
    "\n",
    "#-- plot FFT\n",
    "iax = 1\n",
    "ax[iax].plot(vf, abs(fft_dirac) /max(abs(fft_dirac)) , \"-\", linewidth=1.2, color=\"C0\", label=\"Dirac\")\n",
    "ax[iax].plot(vf, abs(fft_trian) /max(abs(fft_trian)) , \"-\", linewidth=1.2, color=\"k\", label=label1)\n",
    "ax[iax].plot(vf, abs(fft_gauss) /max(abs(fft_gauss)) , \"-\", linewidth=1.2, color=\"C2\", label=label2)\n",
    "ax[iax].plot(vf, abs(fft_elast1)/max(abs(fft_elast1)), \"-\", linewidth=1.75, color=\"C3\", label=label31, zorder=8)\n",
    "ax[iax].plot(vf, abs(fft_elast2)/max(abs(fft_elast2)), \"-\", linewidth=1.2, color=\"C1\", label=label32)   #, dashes=(1,1))\n",
    "ax[iax].plot(vf, abs(fft_elast3)/max(abs(fft_elast3)), \"-\", linewidth=1.2, color=\"C8\", label=label33)\n",
    "#ax[iax].plot(vf, abs(fft_blakm), \"--\", linewidth=1, color=\"C4\", label=label4)\n",
    "#ax[iax].plot(vf, abs(fft_blakm2),\"--\", linewidth=1, color=\"C4\", label=label44, dashes=(1,1))\n",
    "ax[iax].legend()\n",
    "ax[iax].set_xlabel(\"Frequency (Hz)\")\n",
    "#ax[iax].set_ylabel(\"Amplitude\")\n",
    "ax[iax].set_xscale(\"log\")\n",
    "ax[iax].set_yscale(\"log\")\n",
    "ax[iax].set_xlim(1.0,1.25e2)\n",
    "ax[iax].set_ylim(1e-17,1e1)\n",
    "ax[iax].set_yticks([1e-16, 1e-12, 1e-8, 1e-4, 1e0])\n",
    "\n",
    "# common tuning\n",
    "for iax in range(0,len(ax)) :\n",
    "    ax[iax].set_ylabel(\"normalized amplitude\")\n",
    "    \n",
    "# add labels (a), (b)\n",
    "for (iax, label) in zip([0,1], [\"(a)\", \"(b)\"]) :\n",
    "    ax[iax].text(-0.04, 1.08, label, ha='right', va='bottom', transform=ax[iax].transAxes, fontsize=14, fontweight='bold')\n",
    "\n",
    "# final size adjustments\n",
    "fig.tight_layout()\n",
    "plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=0.35)\n",
    "\n",
    "# print figure to file\n",
    "fig.savefig( file_ou + \".png\", format=\"png\", dpi=300)\n",
    "print(file_ou)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
