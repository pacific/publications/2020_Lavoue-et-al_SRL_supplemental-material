{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Generate pseudo-analytical Green functions for train simulations\n",
    "\n",
    "This notebook intends to make the package self-consistent, i.e. independent from simulation softwares (like the SEM46 software used in the paper and developed by the Seiscope consortium, which is not publicly available). It generates *pseudo*-analytical Green functions in the sense that the considered solutions may not be physically relevant (simple Dirac delta functions attenuated by geometrical spreading and central-frequency Q-approximation). These solutions are equivalent to an acoustic pressure (while we discuss ground velocities in the paper). Nonetheless, they are sufficient to reproduce the main spectral characteristics discussed in the paper.\n",
    "\n",
    "François Lavoué (francois.lavoue@univ-grenoble-alpes.fr), 25 March 2020  \n",
    "Last updated François Lavoué, 2 Sept 2020  \n",
    "Copyright (c) 2020, Univ. Grenoble Alpes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "## Contents\n",
    "\n",
    "- Notes\n",
    "\n",
    "- Modules\n",
    "\n",
    "- Define simulation parameters\n",
    "\n",
    "- Define acquisition\n",
    "\n",
    "- Compute simple pseudo-Green functions based on traveltimes between virtual source (actual sensor) and virtual receivers (sleepers = actual sources).\n",
    "\n",
    "- Print to file for further use in the subsequent notebooks `01_*.ipynb`, `02*.ipynb` and `03_*.ipynb`, that will pre-process and convolve these Green functions with source time functions representing the train passage."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "## Notes\n",
    "\n",
    "- This work is licensed under the BSD 3-Clause License (see `LICENSE` file).\n",
    "\n",
    "- Although independent from the Seiscope's libraries, this notebook still follows Seiscope's convention and I/O formats for compatibility with the subsequent notebooks.\n",
    "\n",
    "- This notebook can be converted to a Python script (e.g. for automated execution on a server) using\n",
    "  ```\n",
    "  jupyter nbconvert --to script 00_generate_simple_Green_functions.ipynb\n",
    "  ```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "## Modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Backward compatibility of print() between Python 2 and 3\n",
    "#from __future__ import print_function\n",
    "\n",
    "# Plot figures directly in the notebook\n",
    "%matplotlib notebook\n",
    "\n",
    "# Plot defaults\n",
    "import matplotlib.pyplot as plt\n",
    "plt.style.use('seaborn-talk')                 # Matplotlib style sheet\n",
    "plt.rcParams['figure.figsize'] = 10,5\n",
    "\n",
    "# Font sizes\n",
    "SMALL_SIZE = 10\n",
    "MEDIUM_SIZE = 12\n",
    "BIGGER_SIZE = 14\n",
    "plt.rc('font', size=SMALL_SIZE)          # controls default text sizes\n",
    "plt.rc('axes', titlesize=MEDIUM_SIZE)     # fontsize of the axes title\n",
    "plt.rc('axes', labelsize=SMALL_SIZE)     # fontsize of the x and y labels\n",
    "plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels\n",
    "plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels\n",
    "plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize\n",
    "plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%javascript\n",
    "// Disable Auto-scrolling\n",
    "IPython.OutputArea.prototype._should_scroll = function(lines){return false;}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#-- import general-purpose modules\n",
    "import numpy as np\n",
    "import os\n",
    "import scipy\n",
    "import struct"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Define output format"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "output_format = \"sem46\"   # \"sem46\", \"specfem3d\", or your own format (you may then edit this notebook accordingly)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "## Define simulation parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# medium properties\n",
    "vp = 3400.    # (m/s)\n",
    "vs = 2000.    # (m/s)\n",
    "rho = 2600.   # (kg/m3)\n",
    "qp = 100.\n",
    "qs = 50.\n",
    "\n",
    "fc = 10.0     # (central frequency for Q-attenuation, in Hz)\n",
    "\n",
    "comp = \"P\"   # component to simulate:\n",
    "             #    \"P\" for acoustic pressure,\n",
    "             # or \"S\" for an equivalent of acoustic pressure but using the Vs value above.\n",
    "             # (no other component available for the moment)\n",
    "\n",
    "# simulation parameters (time discretization and duration)\n",
    "dt = 0.004    # (s, i.e. fs = 250 Hz)\n",
    "tmax = 2.0    # (s)\n",
    "\n",
    "#-- acquisition geometry\n",
    "\n",
    "# virtual source location (actual sensor)\n",
    "xsrc = 6000.   # (m)\n",
    "ysrc = 100.    # (m)\n",
    "zsrc = 0.0     # (m)\n",
    "\n",
    "# virtual receiver locations (actual sources, i.e. sleepers)\n",
    "yrec = 400.    # (m, such that dmin = 300 m)\n",
    "zrec = 0.0     # (m)\n",
    "xrec_min = 100.     # (m)\n",
    "xrec_max = 11900.   # (m)\n",
    "\n",
    "dmin = abs(ysrc-yrec)\n",
    "\n",
    "drec = 0.6096   # (m, = 24 inches)\n",
    "\n",
    "regular_or_perturbed_sleepers = \"perturbed\"   # \"regular\" or \"perturbed\"\n",
    "drec_pert = 0.05   # perturbation (in m)\n",
    "\n",
    "if regular_or_perturbed_sleepers == \"regular\" :\n",
    "    suffix_sleepers = (\"regular-drec-%gm\" % drec)\n",
    "elif regular_or_perturbed_sleepers == \"perturbed\" :\n",
    "    suffix_sleepers = (\"perturbed-drec-%gm+-%gcm\" % (drec, drec_pert*100))\n",
    "\n",
    "# output directory\n",
    "if comp == \"S\" :\n",
    "    dir_ou = (\"results/output_custom_run_homo_Vs-%gms_Qs-%g_%s_dmin-%gm\" % (vs, qs, suffix_sleepers, dmin))\n",
    "else :\n",
    "    dir_ou = (\"results/output_custom_run_homo_Vp-%gms_Qp-%g_%s_dmin-%gm\" % (vp, qp, suffix_sleepers, dmin))\n",
    "    \n",
    "if not os.path.isdir(dir_ou):\n",
    "    os.makedirs(dir_ou)\n",
    "else :\n",
    "    print(\"Warning: output dir already exists.\")\n",
    "print(dir_ou)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "## Define sleeper locations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#-- define regular sleeper locations\n",
    "\n",
    "vx_rec = np.arange(xrec_min, xrec_max+0.1*drec, drec)\n",
    "nrec = len(vx_rec)\n",
    "\n",
    "print(\"nb of sleepers = %i\" % nrec)\n",
    "print(\"xmin / xmax sleepers = %f / %f m\" % (vx_rec.min(), vx_rec.max()))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#-- perturb sleeper locations\n",
    "\n",
    "min_pert = np.inf\n",
    "max_pert = -np.inf\n",
    "\n",
    "if regular_or_perturbed_sleepers == \"perturbed\" :\n",
    "    print(\"perturb sleepers by +/- %g m\" % drec_pert)\n",
    "    for irec in range(0,nrec) :\n",
    "        vx_rec[irec] = vx_rec[irec] + drec_pert*2*(0.5-np.random.rand())\n",
    "    print(\"xmin / xmax sleepers = %f / %f m\" % (vx_rec.min(), vx_rec.max()))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#-- print acqui to file for further processing\n",
    "\n",
    "if output_format == \"sem46\" :\n",
    "    file_ou = dir_ou + \"/acqui\"\n",
    "    fid = open(file_ou, \"wt\")\n",
    "    # follow Seiscope convention for acqui files:\n",
    "    #          z        x        y        i1   i2   i0\n",
    "    # where i1 and i2 are not used here, and i0 = 0 for a source and 1 for a receiver\n",
    "    fid.write(\"%10.4f   %10.4f   %10.4f   %i   %i   %i\\n\" % (0.0, xsrc, ysrc, 0, 0, 0))\n",
    "    for irec in range(0,nrec) :\n",
    "        fid.write(\"%10.4f   %10.4f   %10.4f   %i   %i   %i\\n\" % (0.0, vx_rec[irec], yrec, 0, 0, 1))\n",
    "    fid.close()\n",
    "    \n",
    "elif output_format == \"specfem3d\" :\n",
    "    print(\"to be implemented...\")\n",
    "    file_ou1 = dir_ou + \"/DATA/CMT_SOLUTION\"\n",
    "    file_ou2 = dir_ou + \"/DATA/STATION_FILE\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "## Compute analytical Green functions\n",
    "\n",
    "### (assuming an acoustic, homogeneous halfspace)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#-- Compute analytical Green functions\n",
    "# (assuming an acoustic, homogeneous halfspace)\n",
    "\n",
    "import time\n",
    "from scipy import signal\n",
    "\n",
    "# compute distances and P arrival times between source and receivers\n",
    "vdist = np.sqrt( (xsrc-vx_rec)**2 + (ysrc-yrec)**2 + (zsrc-zrec)**2 )\n",
    "if comp == \"P\" :\n",
    "    tarr = vdist/vp\n",
    "elif comp == \"S\" :\n",
    "    tarr = vdist/vs\n",
    "it_arr = np.int_(tarr/dt)\n",
    "\n",
    "print(\"max. %s arrival time = %g s.\" % (comp, tarr.max()))\n",
    "print(\"(reminder: tmax = %g s)\" % (tmax))\n",
    "\n",
    "# check tmax\n",
    "if tmax < tarr.max() :\n",
    "    print(\"\\nError: tmax = %g s < max. arrival time = %g s.\" % (tmax,tarr.max()))\n",
    "    exit\n",
    "\n",
    "if tmax > tarr.max() + 2.0 :\n",
    "    print(\"\\nWarning: tmax = %g s > max. arrival time = %g + 2 s.\" % (tmax,tarr.max()))\n",
    "\n",
    "# define time vector\n",
    "vt = np.arange(0.0, tmax+0.1*dt, dt)\n",
    "nt = len(vt)\n",
    "    \n",
    "# init. data\n",
    "data = np.zeros([nt,nrec])\n",
    "print(data.shape)\n",
    "\n",
    "# compute analytical Green functions\n",
    "# (acoustic pressure in a homogeneous, attenuating halfspace)\n",
    "t0 = time.time()\n",
    "for irec in range(0,nrec) :\n",
    "    \n",
    "    if comp == \"P\" :\n",
    "        # P(r,t) = [ delta(t-t0) / (2 pi r) ] * exp[-pi fc r / (V Q)]\n",
    "        #                Dirac delta                             geometrical spreading     Q-attenuation\n",
    "        data[:,irec] = ( signal.unit_impulse(nt,it_arr[irec]) / (2.*np.pi*vdist[irec]) ) * np.exp(-np.pi*fc*vdist[irec]/(vp*qp))\n",
    "        \n",
    "    elif comp == \"S\" :\n",
    "        data[:,irec] = ( signal.unit_impulse(nt,it_arr[irec]) / (2.*np.pi*vdist[irec]) ) * np.exp(-np.pi*fc*vdist[irec]/(vs*qs))\n",
    "\n",
    "t1 = time.time()\n",
    "print(\"\\ntime for building analytical solutions = %g s (%g min)\" % (t1-t0,(t1-t0)/60))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#-- print synthetic data to file\n",
    "\n",
    "if output_format == \"sem46\" :\n",
    "    file_ou = dir_ou + (\"/seismograms/fsismos_%s0000\" % comp)\n",
    "    # (following SEM46 naming convention)\n",
    "\n",
    "    if not os.path.isdir(dir_ou + \"/seismograms/\"): os.makedirs(dir_ou + \"/seismograms/\")\n",
    "\n",
    "    fid = open(file_ou,\"wb\")\n",
    "    fid.write(struct.pack('f'*len(data.flatten('F')), *data.flatten('F')))\n",
    "    fid.close()\n",
    "\n",
    "elif output_format == \"specfem3d\" :\n",
    "    print(\"to be implemented...\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#-- print simulation parameters to file for further processing\n",
    "\n",
    "if output_format == \"sem46\" :\n",
    "    file_ou = dir_ou + \"/sem_input_namelist\"\n",
    "    fid = open(file_ou, 'wt')\n",
    "    fid.write(\"&FILE_NAME_MODEL Vp_homo=%g, Vs_homo=%g, Den_homo=%g, Vp_name='vp_gll', Vs_name='vs_gll', Den_name='rho_gll' /\\n\" % (vp, vs, rho))\n",
    "    fid.write(\"&FORWARD_PARAM dt=%g, total_nb_time_step=%i /\\n\" % (dt, nt))\n",
    "    fid.write(\"&ATTENUATION_INFO min_freq=0.1, max_freq=7.5, Q_ref=40., Qphomo=%g, Qshomo=%g, Qp_file='qp_gll', Qs_file='qs_gll' /\\n\" % (qp, qs))\n",
    "    fid.close()\n",
    "    \n",
    "elif output_format == \"specfem3d\" :\n",
    "    print(\"to be implemented...\")\n",
    "    \n",
    "elif output_format == \"custom\" :\n",
    "    # (example of what your own format could be)\n",
    "    file_ou = dir_ou + \"/simulation_parameters.txt\"\n",
    "\n",
    "    fid = open(file_ou, 'wt')\n",
    "    fid.write(\"nt = %i\\n\" % nt)\n",
    "    fid.write(\"dt = %f s\\n\" % dt)\n",
    "    if comp == \"P\" :\n",
    "        fid.write(\"vp = %f m/s\\n\" % vp)\n",
    "        fid.write(\"qp = %f\\n\" % qp)\n",
    "    elif comp == \"S\" :\n",
    "        fid.write(\"vs = %f m/s\\n\" % vs)\n",
    "        fid.write(\"qs = %f\\n\" % qs)\n",
    "    fid.close()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#-- print source signal to file\n",
    "# (Dirac delta, used for check and comparison in subsequent notebooks)\n",
    "\n",
    "source = signal.unit_impulse(nt,0)\n",
    "\n",
    "if output_format == \"sem46\" :\n",
    "    file_ou = dir_ou + \"/fsource\"\n",
    "\n",
    "    fid = open(file_ou,\"wb\")\n",
    "    fid.write(struct.pack('f'*len(source.flatten('F')), *source.flatten('F')))\n",
    "    fid.close()\n",
    "    \n",
    "elif output_format == \"specfem3d\" :\n",
    "    print(\"to be implemented...\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
