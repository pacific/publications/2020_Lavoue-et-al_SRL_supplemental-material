Published figures are under SSA copyright and will be available here
after a 6-month embargo period. In the meantime, you may reproduce them
with the notebooks.

Francois Lavoue (francois.lavoue@univ-grenoble-alpes.fr), 3 Sept 2020
