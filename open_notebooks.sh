#!/bin/bash

## activate conda environment
## (may also be done in your .bashrc or in command line)
#conda activate your_conda_environment

# launch notebook server and redirect stdout and stderr to log files
mkdir -p logs
jupyter-notebook >logs/log_notebooks.out 2>logs/log_notebooks.err &

## try using more memory (you may tune this according to your specs,
## but it does not seem to work great for me [FL, on a MacBook Pro 15"])
#jupyter-notebook --NotebookApp.max_body_size=25769803776 --NotebookApp.max_buffer_size=25769803776 >logs/log_notebooks.out 2>logs/log_notebooks.err &

